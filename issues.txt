#1  Have an issue tracking system [closed]

    I want to be able to defer things that need fixing without
    littering todos accross my code or needing to open a browser. This
    txt file will do for now, as I am the only programmer on the
    project and there will be no more programmers on this project. I
    shouldn't learn org-mode or GNAT at the moment. I'll keep it in
    git.

    There is one # per issue, with tags in [brackets]. If I do this in
    the future, such a consistent format would make it straightforward
    to convert to CSV to be imported into gitlab, if necessary:

    https://docs.gitlab.com/ee/user/project/issues/csv_import.html

    At some point it may be nice to make a local editor for gitlab
    issues:

    https://docs.gitlab.com/ee/api/issues.html#new-issue
    https://docs.gitlab.com/ee/api/graphql/reference/#issue

#2  Choose an http client [closed]

    http://www.arewewebyet.org/topics/clients/

    "ureq is not the only simple HTTP client in existence. attohttpc,
    minreq and http_req all fill a similar niche." -- Shnatsel, reddit.

    I was still surprised by how many dependencies ureq pulled in! It
    would be nice to look at the alternatives above, but ureq should
    satisfy my needs.

    [1] https://www.reddit.com/r/rust/comments/eu6qg8/future_of_ureq_http_client_library/ffofnb9/

#3 Learn ureq [closed]

   ureq documentation is here: https://docs.rs/ureq/0.12.1/ureq/

   Was quite confused because I was using `cargo check` in entr
   instead of using `cargo build`, so running the executable was
   producing no change. 

#4 Parse GET response into definition [open]

#5 Open compare ureq with minreq [open]

   After this utility is written, open a new branch and compare the
   two libraries.
   
