extern crate ureq;
use std::env;

struct WiktAnchor {
    // anchor tag that we'd see from wiktionary. Not sure how I'd use str.
    rel: String,
    href: String,
    title: String, // This ends up being the relevant substring.
    span: String,
}

fn main() {
    let args: Vec<String> = env::args().collect(); // What does collect do to iterators?

    // TODO: sanitize input word. I accidentally used args[0], which resulted in a bad response.
    let query_word = &args[1];

    println!("Looking for definition of word {}", query_word);
    let url = "https://en.wiktionary.org/api/rest_v1/page/definition/";
    let testurl = url.to_owned() + query_word; // why didn't this need "to owned?

    // get() returns a request, call() runs that request
    println!("making request");
    let response = ureq::get(&testurl).call();

    if response.ok() {
        println!("response is ok");
        let s = response.into_string().unwrap();
        let s = remove_tags(s); // it would be nice to be able to do this while
                                // retaining json.
        println!("{}", s);
    } else {
        println!("something is not ok.");
    }
}

fn remove_tags(response: String) -> String {
    let mut am_outside_tag: bool = true; // Store state.
    let mut out_string: String = "".to_string();

    for c in response.chars() {
        // Update state
        match c {
            // I am somewhat surprised that this works. Reading the rust reference
            // (which I'm starting to prefer to the rust book), somehow inspired me
            // to write this block.
            '<' => { am_outside_tag = false; }
            '>' => { am_outside_tag = true; continue; }
            _ => (),
        };

        if am_outside_tag {
            out_string += &c.to_string(); // Still not sure why I needed &.
        }
    }
    out_string
}
